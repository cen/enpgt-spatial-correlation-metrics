import os
import re
import shutil
import sys
from collections.abc import Iterable
from datetime import datetime

import numpy as np
from tqdm import tqdm
from vincenty import vincenty


def get_distance_matrix(id_to_latlng: dict[str, tuple[float, float]]):
    return np.asarray([[vincenty(p1, p2) for p2 in id_to_latlng.values()] for p1 in id_to_latlng.values()])


def get_distance_bands(distance_matrix):  # TODO: typing
    distance_matrix_mod = distance_matrix.copy()
    np.fill_diagonal(distance_matrix_mod, np.inf)
    min_max_nearest = distance_matrix_mod.min(axis=0).max(axis=0)

    distance_matrix_mod[distance_matrix_mod > min_max_nearest] = 0
    distance_matrix_mod[distance_matrix_mod > 0] = 1
    distance_matrix_mod = distance_matrix_mod.astype(int)

    distances = np.multiply(distance_matrix, distance_matrix_mod)
    invd1 = np.divide(1, distances, out=np.zeros_like(distances), where=distances != 0)

    return distance_matrix_mod, invd1


def output_matrix(file_path: str, matrix: np.ndarray, keys: Iterable[str]):
    with open(file_path, "w") as dist_f:
        dist_f.write("," + ",".join(keys) + "\n")
        for i, geo_id in enumerate(keys):
            dist_f.write(geo_id + ",")
            dist_f.write(",".join(str(dist) for dist in matrix[i]))
            dist_f.write("\n")


def get_spatial_correlation_metrics(infile: str, output_dir: str):
    print("---")
    print(f"Getting Spatial Correlation metrics for file {infile}.")

    shutil.copyfile(infile, os.path.join(output_dir, os.path.basename(infile)))

    num_lines = sum(1 for _ in open(infile))

    id_to_latlng = {}

    with open(infile) as task_file:
        for i, line in enumerate(tqdm(task_file, total=num_lines, desc="Parsing Task File.")):
            line = line.strip()
            line_split = re.split("[;,\\t]", line)

            try:
                id_to_latlng[line_split[0]] = (float(line_split[1]), float(line_split[2]))
            except Exception:
                print(f"Could not interpet line: {line}")

    distance_matrix = get_distance_matrix(id_to_latlng)
    distance_matrix_inv = np.divide(1, distance_matrix, out=np.zeros_like(distance_matrix), where=distance_matrix != 0)
    np.fill_diagonal(distance_matrix_inv, 0)

    output_matrix(os.path.join(output_dir, "distance_matrix.csv"), distance_matrix, id_to_latlng.keys())

    distance_band_neighbor_matrix, inverse_weights_matrix = get_distance_bands(distance_matrix)

    output_matrix(
        os.path.join(output_dir, "distance_band_neighbors.csv"), distance_band_neighbor_matrix, id_to_latlng.keys()
    )

    output_matrix(
        os.path.join(output_dir, "distance_band_inverse_weights.csv"),
        inverse_weights_matrix,
        id_to_latlng.keys(),
    )

    clamped_distance_matrix = distance_band_neighbor_matrix * distance_matrix

    output_matrix(os.path.join(output_dir, "clamped_distance_matrix.csv"), clamped_distance_matrix, id_to_latlng.keys())


def main(args):
    default_task_file = "task.txt"

    for arg in args:
        if os.path.isfile(arg):
            default_task_file = arg

        else:
            print(f"Could not find file {arg}.")
            return

    if not os.path.isfile(default_task_file):
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        return

    output_dir = os.path.join("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))
    os.makedirs(output_dir)

    get_spatial_correlation_metrics(default_task_file, output_dir)

    # output_readme(os.path.join(output_dir, "readme.txt"))


if __name__ == "__main__":
    main(sys.argv[1:])
